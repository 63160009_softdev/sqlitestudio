--
-- File generated with SQLiteStudio v3.3.3 on Thu Sep 8 16:44:55 2022
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Category
CREATE TABLE Category (category_id VARCHAR NOT NULL PRIMARY KEY UNIQUE, category_name STRING NOT NULL);

-- Table: Customer
CREATE TABLE Customer (customer_id VARCHAR PRIMARY KEY UNIQUE NOT NULL, email STRING NOT NULL, firstname STRING NOT NULL, lastname STRING NOT NULL, phone VARCHAR NOT NULL, address STRING NOT NULL);
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C01', 'Jakkapan@gmail.com', 'Jakkapan', 'Guntima', '062xxxxxxx', '642/13 Bangsean Chonburi 20130');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C02', 'Pongphat@gmail.com', 'Pongphat', 'Chusak', '098xxxxxxx', '207 Samed Chonburi 20000');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C03', 'Manee@gmail.com', 'Manee', 'Meemor', '064xxxxxxx', '209/5 Angsila Chonburi 20000');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C04', 'Kanteera@gmail.com', 'Kanteera', 'Yimsuk', '062xxxxxxx', '296 Phanat Nikhom Chonburi 20140');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C05', 'Pimon@gmail.com', 'Pimon', 'Nongyao', '081xxxxxxx', '608/17 Bangsean Chonburi 20130');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C06', 'Mongkol@gmail.com', 'Mongkol', 'Kodphat', '089xxxxxxx', '17/4 Bang Lamung Chonburi 20150');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C07', 'Narinporn@gmail.com', 'Narinporn', 'Oonwhan', '090xxxxxxx', '666 Ban Nong Kai Thuean Chonburi 20140');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C08', 'Yanisa@gmail.com', 'Yanisa', 'Chokdee', '063xxxxxxx', '147/6 Bangsean Chonburi 20130');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C09', 'Sodsai@gmail.com', 'Sodsai', 'Meejai', '061xxxxxxx', '303 Bang Lamung Chonburi 20150');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C10', 'KobKob@gmail.com', 'KobKob', 'Ooboob', '091xxxxxxx', '56/12 Angsila Chonburi 20000');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C11', 'Tyga@gmail.com', 'Tyga', 'Loco', '068xxxxxxx', '777/69 Bangsean Chonburi 20130');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C12', 'Balvin@gmail.com', 'Balvin', 'Jay', '098xxxxxxx', '333 Samed Chonburi 20130');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C13', 'Bizzie@gmail.com', 'Bizzie', 'Jozo', '069xxxxxxx', '265/5 Angsila Chonburi 20000');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C14', 'God@gmail.com', 'God', 'Haland', '087xxxxxxx', '201 Kabin Ubonratchathani 34270');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C15', 'Sanchoo@gmail.com', 'Sanchoo', 'Booboo', '091xxxxxxx', '77 Angsila Chonburi 20000');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C16', 'Covid@gmail.com', 'Covid', 'Nineteen', '095xxxxxxx', '888/14 Ban Nong Kai Thuean Chonburi 20140');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C17', 'Jassi@gmail.com', 'Jassi', 'Jlingz', '063xxxxxxx', '125/15 Klang Rayong 21000');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C18', 'Kevin@gmail.com', 'Kavin', 'Boys', '066xxxxxxx', '54/1 Kaochamao Rayong 21110');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C19', 'Beam@gmail.com', 'Beam', 'Thamsong', '080xxxxxxx', '9/32 Kohkood Trat 23000');
INSERT INTO Customer (customer_id, email, firstname, lastname, phone, address) VALUES ('C20', 'Ureerat@gmail.com', 'Ureerat', 'Khottae', '088xxxxxxx', '11 Bo Thong Chonburi 20270');

-- Table: Employee
CREATE TABLE Employee ("employee_id(PK)" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, firstname STRING NOT NULL, lastname STRING NOT NULL, date_of_attendance DATE NOT NULL, date_of_birth DATE NOT NULL, gender CHAR NOT NULL, address STRING NOT NULL, phone STRING NOT NULL, salary INTEGER NOT NULL);
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (1, 'kong', 'Jamsai', '2/5/2020', '27/05/1998', 'Male', '77/19 Bangsean Chonburi 20130', '083xxxxxxx', '20,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (2, 'Yim', 'Boonplok', '7/5/2020', '5/6/1996', 'Female', '205 Samed Chonburi 20000', '094xxxxxxx', '20,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (3, 'Ubon', 'Romyen', '2/6/2020', '27/02/1999', 'Male', '208/5 Angsila Chonburi 20000', '080xxxxxxx', '18,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (4, 'Lookpla', 'Yimyam', '11/6/2021', '26/06/2000', 'Female', '203/18 Phanat Nikhom Chonburi 20140', '086xxxxxxx', '18,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (5, 'Pariyakorn', 'Bankate', '16/06/2021', '23/05/2001', 'Female', '605/17 Bangsean Chonburi 20130', '065xxxxxxx', '18,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (6, 'Nonanee', 'Rodma', '20/08/2021', '5/2/2000', 'Male', '17/5 Bang Lamung Chonburi 20150', '090xxxxxxx', '15,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (7, 'Paowaree', 'Sukjai', '5/9/2021', '14/03/2001', 'Female', '111 Ban Nong Kai Thuean Chonburi 20140', '081xxxxxxx', '15,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (8, 'Montree', 'Meesuk', '7/9/2021', '12/11/2001', 'Male', '205/17 Bangsean Chonburi 20130', '098xxxxxxx', '15,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (9, 'Somsri', 'Rakying', '15/10/2021', '25/08/2000', 'Female', '208 Bang Lamung Chonburi 20150', '067xxxxxxx', '15,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (10, 'Phadol', 'Wongsiri', '23/10/2021', '17/03/2000', 'Male', '144 Angsila Chonburi 20000', '084xxxxxxx', '15,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (11, 'Kuntapong', 'Deejai', '8/11/2021', '2/4/2001', 'Male', '204 Phanat Nikhom Chonburi 20140', '093xxxxxxx', '15,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (12, 'Duangporn', 'Jubjub', '8/11/2021', '8/4/2001', 'Female', '157 Thamai Chanthaburi 22120', '092xxxxxxx', '15,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (13, 'Athinun', 'Meethong', '10/12/2021', '8/10/2001', 'Male', '567 Bangpakong Chachoengsao 24150', '099xxxxxxx', '15,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (14, 'Janjira', 'Phangam', '1/1/2022', '22/06/2000', 'Female', '148 Angsila Chonburi 20000', '087xxxxxxx', '12,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (15, 'Wanida', 'Jairak', '5/1/2022', '19/10/2000', 'Female', '655 Ban Nong Kai Thuean Chonburi 20140', '091xxxxxxx', '12,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (16, 'Anurak', 'Thongkam', '11/2/2022', '20/09/2002', 'Male', '208/15 Klang Rayong 21000', '095xxxxxxx', '12,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (17, 'Pimsa', 'Nakhamhai', '24/02/2022', '25/07/2001', 'Female', '514 Kaochamao Rayong 21110', '063xxxxxxx', '12,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (18, 'Phathiphan', 'Waiprib', '24/04/2022', '30/12/2002', 'Male', '999 Kohkood Trat 23000', '066xxxxxxx', '12,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (19, 'Takkatan', 'Chonlapratarn', '1/5/2022', '9/11/2001', 'Female', '11/1 Bo Thong Chonburi 20270', '080xxxxxxx', '12,000');
INSERT INTO Employee ("employee_id(PK)", firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, salary) VALUES (20, 'Phet', 'Sahaphat', '10/5/2022', '19/08/2001', 'Male', '88 Ko Chan Chonburi 20240', '089xxxxxxx', '12,000');

-- Table: Order
CREATE TABLE "Order" (order_id INTEGER PRIMARY KEY NOT NULL UNIQUE, product_name STRING NOT NULL, total_price INTEGER NOT NULL, date DATE NOT NULL, total_discount INTEGER NOT NULL, employee_id REFERENCES Employee ("employee_id(PK)") ON UPDATE CASCADE, customer_id REFERENCES Customer (customer_id) ON UPDATE CASCADE);
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (1, 'Coffee', 50, '1/9/2022', 5, '1', 'C01');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (2, 'Americano', 50, '2/9/2022', 5, '2', 'C02');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (3, 'Fruite Cake', 75, '2/9/2022', 10, '3', 'C03');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (4, 'Cappuccino', 50, '3/9/2022', 5, '4', 'C04');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (5, 'Cocoa', 35, '3/9/2022', 5, '5', 'C05');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (6, 'Black Tea', 45, '3/9/2022', 5, '6', 'C06');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (7, 'Orange Juice', 35, '4/9/2022', 5, '7', 'C07');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (8, 'Fried Rice', 60, '5/9/2022', 5, '8', 'C08');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (9, 'Water', 10, '5/9/2022', 3, '9', 'C09');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (10, 'Cola', 25, '6/9/2022', 5, '10', 'C10');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (11, 'Vanilla Cake', 65, '6/9/2022', 5, '11', 'C11');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (12, 'Omelette', 50, '6/9/2022', 5, '12', 'C12');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (13, 'Mocca', 55, '6/9/2022', 5, '13', 'C13');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (14, 'Ice Coffee', 55, '6/9/2022', 5, '14', 'C14');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (15, 'Smoothies Berry', 65, '6/9/2022', 5, '15', 'C15');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (16, 'Chocolate Cake', 75, '6/9/2022', 5, '16', 'C16');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (17, 'Spaghetti', 129, '6/9/2022', 10, '17', 'C17');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (18, 'Slad', 105, '6/9/2022', 5, '18', 'C18');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (19, 'Macaron', 125, '6/9/2022', 5, '19', 'C19');
INSERT INTO "Order" (order_id, product_name, total_price, date, total_discount, employee_id, customer_id) VALUES (20, 'Latte', 65, '6/9/2022', 5, '20', 'C20');

-- Table: Product
CREATE TABLE Product (product_id VARCHAR PRIMARY KEY UNIQUE NOT NULL, product_name STRING NOT NULL, price INTEGER NOT NULL, category_id VARCHAR NOT NULL REFERENCES Category (category_id) ON UPDATE CASCADE);

-- Table: Quantity
CREATE TABLE Quantity (product_id VARCHAR NOT NULL REFERENCES Product (product_id) ON UPDATE CASCADE, order_id INTEGER NOT NULL REFERENCES "Order" (order_id) ON UPDATE CASCADE, quantity INTEGER NOT NULL, PRIMARY KEY (product_id, order_id), UNIQUE (product_id, order_id));
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P01', 1, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P02', 2, 2);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P03', 3, 2);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P04', 4, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P05', 5, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P06', 6, 3);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P07', 7, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P08', 8, 2);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P09', 9, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P10', 10, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P11', 11, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P12', 12, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P13', 13, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P14', 14, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P15', 15, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P16', 16, 2);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P17', 17, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P18', 18, 1);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P19', 19, 2);
INSERT INTO Quantity (product_id, order_id, quantity) VALUES ('P20', 20, 3);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
